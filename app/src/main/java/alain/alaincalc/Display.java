package alain.alaincalc;

import java.text.* ;
import java.util.Locale;

import android.widget.TextView;

/**
 * Created by Diamond on 25/08/2017.
 */

public class Display {
    private TextView textView;
    private boolean limpaDisplay = true ;
    NumberFormat nf = NumberFormat.getInstance() ;

    public Display(TextView display) {
        this.textView = display;
    }

    public TextView getTextView() {
        return textView;
    }

    public boolean isLimpaDisplay() {
        return limpaDisplay;
    }

    public void setLimpaDisplay(boolean limpaDisplay) {
        this.limpaDisplay = limpaDisplay;
    }

    public String getText(){
        return  this.textView.getText().toString() ;
    }


    public void setText(String texto) {

        this.textView.setText(texto);
    }

    public void setText(double valor){
        this.setText(nf.format(valor));
    }


    public double getValue() {
        return Double.parseDouble(this.getText());
    }
}
